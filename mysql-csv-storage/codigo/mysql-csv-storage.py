# Autor: Jose Poblete  -> Practicante Alianzas Estrategica

import json
from datetime import datetime
from google.cloud import storage, bigquery
import mysql.connector
from mysql.connector import errorcode
from google_auth_oauthlib import flow
import os
import csv
import time
import threading

# file_name: nombre del archivo de destino. Este siempre sera .csv sin importar la extension que le pongas (se extrae el nombre sin extension)
# query: consulta a correr en mysql a correr
# obtiene los datos desde el servidor especificado en config.json
# guarda los datos obtenido en <file_name>.csv
def mysql_to_csv(file_name, query):
    continuar = False
    try:
        config = ""
        with open('config.json', 'r') as file:
            parsed_json = json.load(file)

        config = parsed_json['mysql_conf']

    # ##TEST PUBLIC MYSQL SERVER
        # config = {
        # "user": "rfamro",
        # "password": "",
        # "host": "mysql-rfam-public.ebi.ac.uk",
        # "port":4497,
        # "database": "Rfam",
        # "raise_on_warnings": True
    #   }

        chunk = 70000
        

        cnx = mysql.connector.connect(**config)
        cursor = cnx.cursor()
        print(query)
        if 'from' in query:
            aux_query = 'SELECT FOUND_ROWS() FROM' + query.split("from")[1]
        else:
            aux_query = 'SELECT FOUND_ROWS() FROM' + query.split("FROM")[1]
        print(aux_query)
        print("EJECUTANDO QUERY...")
        cursor.execute(aux_query)
        n_rows = len(cursor.fetchall())
        inp = input(
            "SE EXPORTARAN {} FILAS. DESEA CONTINUAR?(y/n): ".format(n_rows))
        if inp.upper() == 'Y':
            continuar = True
        if continuar:
            if os.path.exists(file_name):
                os.remove(file_name)
            continuar = False
            cursor.execute(query)
            # this will extract row headers
            row_headers = [x[0] for x in cursor.description]
            with open(file_name, "a", newline='') as file:
                file_writer = csv.writer(
                    file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                file_writer.writerow(row_headers)

            row = min(chunk, n_rows)
            i = 0
            perc = 0
            while row <= n_rows:
                rv = cursor.fetchmany(chunk)
                if round((i/n_rows)*100)-perc > 0:
                    perc = round((i/n_rows)*100)
                    print("PROCESADO {}%".format(perc))
                for result in rv:
                    i += 1
                    res2 = []
                    for r in result:
                        if isinstance(r, datetime):
                            r = r.strftime("%m/%d/%Y, %H:%M:%S")
                        res2.append(r)
                    with open(file_name, 'a', newline='') as file:
                        file_writer = csv.writer(
                            file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        file_writer.writerow(res2)

                if(row == n_rows):
                    row += 100
                else:
                    row = min(row+chunk, n_rows)
            cursor.close()
            continuar = True
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        print("Closing...")
        cnx.close()
    return continuar


# storage_credentials_path: Path al archivo JSON que contiene las credenciales de service_account de storage
# bucketName: Nombre del segmento en cloud storage donde se subira el archivo
# project_name: Nombre del projecto de destino en BigQuery
# file_name: nombre del archivo a subir. Este debe estar en la carpeta codigo
# Sube el archivo a cloud storage utilizando las credenciales proporcionadas en el archivo
#         json (apis y servicios -> credenciales -> crear credenciales -> cuenta de servicio)
def upload_storage(storage_credentials_path, bucketName, project_name, file_name):
    print("SUBIENDO A STORAGE...")
    try:
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = storage_credentials_path
        client = storage.Client(project=project_name)
        bucket = client.get_bucket(bucketName)
        blob = bucket.blob(file_name, chunk_size=262144)
        blob.upload_from_filename(file_name)
        print("EL ARCHIVO SE SUBIO CORRECTAMENTE")
        files = bucket.list_blobs()
        fileList = [file.name for file in files if '.' in file.name]
        print("ARCHIVOS EN EL BUCKET: {}".format(fileList))
    except Exception as e:
        print("OCURRIO UN ERROR: {}".format(e))


def thread_function(total_time, start_time):
    aux = total_time - (time.time() - start_time)
    while aux >= 0:
        aux = round(total_time - (time.time() - start_time))
        print("TIEMPO RESTANTE: {} segundos".format(aux))
        time.sleep(1)


if __name__ == "__main__":
    print("\n\n\n-------------- creado por Jose Poblete C. --> Practicante Alianzas Estrategicas ----------------\n\n\n")
    aux = True
    print("QUE DESEAS HACER? \n 1) REALIZAR SOLO LA BAJADA A CSV \n 2) SOLO SUBIR UN ARCHIVO A STORAGE \n 3) BAJAR ARCHIVO Y SUBIR A STORAGE \n 4) SALIR")
    option = input("INGRESE LA OPCION: ")
    if option in ['1', '2', '3']:
        parsed_json = ''
        with open('config.json', 'r') as file:
            parsed_json = json.load(file)

            query = False
            while query == False and option in ['1', '3']:
                query = input(
                    "INGRESA LA QUERY A CORRER (exit para salir) (ej: SELECT * FROM taxonomy LIMIT 10) (enter para usar el archivo config.json): ")
                if query != '' and query != 'exit':
                    aux = query.upper()
                    if 'FROM' not in aux or 'SELECT' not in aux or len(aux.split(' ')) < 3 or len(aux.split('FROM')[0].replace(' ', '')) < 7:
                        print("QUERY INCORRECTA, PRUEBE OTRA VEZ")
                        query = False
            if query != 'exit':
                if query == '':
                    query = parsed_json["query"]
                    print(query)
                if aux:
                    if option in ['2', '3']:
                        storage_credential_location = input(
                            "INGRESA EL PATH AL ARCHIVO JSON CON LAS CREDENTIALES service_account (exit para salir) (enter para usar el archivo config.json): ")
                        if storage_credential_location != 'exit':
                            if storage_credential_location == '':
                                storage_credential_location = parsed_json["storage_credentials_path"]
                            print(storage_credential_location)
                            bucket_name = input(
                                "INGRESA EL NOMBRE DEL BUCKET EN STORAGE DONDE SE SUBIRA EL CSV (exit para salir)(enter para usar el archivo config.json): ")

                            if bucket_name != 'exit':
                                if bucket_name == '':
                                    bucket_name = parsed_json['bucket_name']
                                print(bucket_name)
                                project_name = input(
                                    "INGRESE EL NOMBRE DEL PROYECTO EN BIGQUERY (exit para salir) (ej:so-cm-alianalytics-dev)(enter para usar el archivo config.json): ")
                                if project_name != 'exit':
                                    if project_name == '':
                                        project_name = parsed_json["project_name"]
                                    print(project_name)
                                else:
                                    aux = False
                            else:
                                aux = False
                        else:
                            aux = False

                    if aux:
                        file_name = ''
                        if option in ['1', '3']:
                            file_name = input(
                                "INGRESA EL NOMBRE DEL CSV DE DESTINO (exit para salir)(enter para usar el archivo config.json): ")
                        else:
                            file_name = input(
                                "INGRESA EL NOMBRE DEL CSV A SUBIR (exit para salir)(enter para usar el archivo config.json): ")
                        if file_name != 'exit':
                            if file_name == '':
                                file_name = parsed_json['destination_csv_name']
                            file_name = file_name.split('.')[0] + '.csv'
                            print(file_name)
                            start_time = time.time()
                            cont = True
                            if option in ['1', '3']:
                                cont = False
                                cont = mysql_to_csv(file_name, query)
                            if cont:
                                if option in ['1', '3']:
                                    print("DATOS EXPORTADOS CORRECTAMENTE")
                                if option in ['2', '3']:
                                    size = os.path.getsize(file_name)
                                    print("SE SUBIRAN {}mb.  Tiempo estimado: {} segundos".format(
                                        round(size/1000000), round(size/1000000)*7))
                                    time_t = threading.Thread(target=thread_function, args=(
                                        round(size/1000000)*7, time.time()))
                                    time_t.daemon = True
                                    time_t.start()
                                    upload_storage(storage_credential_location, bucket_name,
                                                   project_name, file_name)
                            print("TIEMPO TOTAL: %s seg ---" %
                                  (time.time() - start_time))

    print("PROGRAMA TERMINADO")
