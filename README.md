# LATAM

En este repositorio se almacena codigo realizado en python que interacutan con bigquery, storage y archivos csv.

Consta de 3 carpetas:

1. BQ-CSV-STORAGE: contiene un script de python con el que se pueden ejecutar 3 funciones
    a. Realizar una bajada desde bigquery a csv local
    b. Subir un archivo csv local a storage
    c. Realizar una bajada desde bigquery cuyo resultado se guarda en un csv en storage

2. MYSQL-CSV-STORAGE: contiene un script de python con el que se pueden ejecutar 3 funciones
    a. Realizar una bajada desde algun servidor mysql a csv local
    b. Subir un archivo csv local a storage
    c. Realizar una bajada desde un servidor mysql a un csv local y subir el csv a storage

3. EXCEL-CSV: contiene una macros de excel para convertir un excel de multiples paginas en varios csv (un csv por pagina)