Autor: Jose Poblete C. --> Practicante Alianzas Estrategicas

Este programa puede realizar 3 tipos de trabajos:

1) Realizar una bajada desde bigquery a csv local
2) Subir un archivo csv local a storage
3) Realizar una bajada desde bigquery y guardar el resultado en storage (se usa una tabla intermedia que al final del proceso es borrada)


Tambien se adiciona un tutorial para generar una cloud function gatillada por la subida a storage, la que genera una tabla siguiendo el esquema del archivo schema.json

0) SI NO CUENTAS CON PYTHON: instalar python --> OJO: AL INSTALAR SETEAR CUSTOMIZATION INSTALATION Y LUEGO SELECCIONAR TODAS LAS OPCIONES EN LAS VENTANAS SIGUIENTES

1) Abrir powershell de windows o equivalente en la carpeta 'codigo' 

2) Correr comando: python -m pip install -r requirements.txt --user

3) Obtener las credenciales de cuenta de servicio (para alianzas se obtiene de aca https://console.cloud.google.com/apis/credentials/serviceaccountkey?_ga=2.145677747.623677338.1585763037-692047827.1577721825&project=so-cm-alianalytics-dev&folder&organizationId=766905427223. ). Para otros, se puede obtener de aca https://console.cloud.google.com/apis  --> credentials

4) Obtener las credenciales OAUTH de bigquery y descargar en un archivo JSON. Se puede obtener de aca https://console.cloud.google.com/apis  --> credentials

5) RENOMBRAR ambos archivos JSON y colocarlos en la carpeta credentials

6) Setear el archivo config.json con la informacion solicitada
7) Ahora solo basta correr el script y seguir los pasos









