# Autor: Jose Poblete  -> Practicante Alianzas Estrategica

from google.cloud import bigquery
import json
from datetime import datetime
from google.cloud import storage, bigquery
from google_auth_oauthlib import flow
import os
import csv
import time
import threading


# bq_credentials_path: path al archivo JSON con las credenciales OAuth 2.0 Client
# project_name: nombre del proyecto en bigquery de donde se extraeran los datos
# file_name: nombre del archivo de destino. Este siempre sera .csv sin importar la extension que le pongas (se extrae el nombre sin extension)
# query: consulta a correr en mysql a correr
# obtiene los datos desde el servidor especificado en config.json
# guarda los datos obtenido en <file_name>.csv
def bq_to_csv(bq_credentials_path, project_name, file_name, query):
    print("DEBES TENER TU GMAIL LATAM ABIERTO PARA CONTINUAR")
    print("OBTENIENDO LA QUERY")
    try:
        inp = input("DESEAS CORRER LA QUERY EN BIGQUERY? (y/n): ")
        if inp == "y":
            if ";" not in query:
                query += ";"
            if ("SELECT" in query or "select" in query) and ("FROM" in query or "from" in query) and 'exit' not in query:
                # TODO: Uncomment the line below to set the `launch_browser` variable.
                launch_browser = True
                #
                # The `launch_browser` boolean variable indicates if a local server is used
                # as the callback URL in the auth flow. A value of `True` is recommended,
                # but a local server does not work if accessing the application remotely,
                # such as over SSH or from a remote Jupyter notebook.

                appflow = flow.InstalledAppFlow.from_client_secrets_file(
                    bq_credentials_path,
                    scopes=['https://www.googleapis.com/auth/bigquery'])

                if launch_browser:
                    appflow.run_local_server()
                else:
                    appflow.run_console()

                credentials = appflow.credentials

                # TODO: Uncomment the line below to set the `project` variable.
                project = project_name
                #
                # The `project` variable defines the project to be billed for query
                # processing. The user must have the bigquery.jobs.create permission on
                # this project to run a query. See:
                # https://cloud.google.com/bigquery/docs/access-control#permissions

                client = bigquery.Client(
                    project=project, credentials=credentials)

                query_string = """""" + query + """"""
                # query_string = """SELECT * FROM AAEE_Volado.VOLADO_PARTI WHERE flown_date BETWEEN '2019-01-01' and '2019-01-01';
                # """
                print("CORRIENDO LA QUERY....")
                query_job = client.query(query_string)

                # Print the results.
                f_name = file_name
                if os.path.exists(f_name):
                    os.remove(f_name)
                i = 0
                start = 0
                n_rows = query_job.result().total_rows
                accept = input(
                    "SE GUARDARAN {} FILAS EN EL ARCHIVO {}, DESEAS CONTINUAR? (y/n): ".format(n_rows, f_name))
                if accept == "y":
                    start = time.time()
                    percent = 0
                    with open(f_name, 'a', newline='') as myfile:
                        for row in query_job.result():
                            if i == 0:
                                print("0%")
                                wr = csv.writer(myfile, quoting=csv.QUOTE_NONE)
                                wr.writerow(list(row.keys()))
                            else:
                                wr = csv.writer(myfile, quoting=csv.QUOTE_NONE)
                                valores = list(row.values())
                                v = []
                                for a in valores:
                                    if a and isinstance(a, str):
                                        b = a.replace(",", "")
                                        v.append(b)
                                    else:
                                        v.append(a)

                                wr.writerow(v)
                                if(i/n_rows*100-percent > 1):
                                    percent = i/n_rows*100
                                    print("{}%".format(round(percent)))
                            i += 1
                    end = time.time()
                    print("Listo")
                    if (end-start)/60 < 1:
                        print("Tiempo Total de descarga: {} segundos".format(
                            (end-start)))
                    else:
                        print("Tiempo Total de descarga: {} minutos".format(
                            (end-start)/60))
                    return True
                else:
                    print("Salio")

                # Wait for the job to complete.
            else:
                if('exit' in query):
                    print("Salio")
                else:
                    print("query incorrecta")
        else:
            print("Salio")
    except Exception as e:
        print("OCURRIO UN ERROR: {}".format(e))


def bq_to_storage(storage_credentials_path, bucket_name, project_name, query):
    print("OBTENIENDO LA QUERY")
    try:
        inp = input("DESEAS CORRER LA QUERY EN BIGQUERY? (y/n): ")
        if inp == "y":
            if ";" not in query:
                query += ";"
            if ("SELECT" in query or "select" in query) and ("FROM" in query or "from" in query) and 'exit' not in query:
                with open('config.json', 'r') as file:
                    parsed_json = json.load(file)

                project = project_name
                client = bigquery.Client.from_service_account_json(parsed_json['storage_credentials_path'])

                query_string = """""" + query + """"""
                print("CORRIENDO LA QUERY....")
                job_config = bigquery.QueryJobConfig()
              
                table_ref = client.dataset(parsed_json['dataset_name_destination']).table(
                    parsed_json['table_name_destination'])
                job_config.destination = table_ref
                # Creates the table wit
                job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE
                query_job = client.query(
                    query_string, location='US', job_config=job_config)
                query_job.result()
                print('Query results loaded to table {}'.format(table_ref.path))

                destination_uri = "gs://" + \
                    bucket_name+"/{}".format(file_name)
                dataset_ref = client.dataset(
                    parsed_json['dataset_name_destination'], project=project)
                table_ref = dataset_ref.table(
                    parsed_json['table_name_destination'])

                extract_job = client.extract_table(
                    table_ref,
                    destination_uri,
                    location='US')
                extract_job.result()  # Extracts results to the GCS

                print('Query results extracted to GCS: {}'.format(destination_uri))

                client.delete_table(table_ref)  # Deletes table in BQ

                print('Table {} deleted'.format(parsed_json['table_name_destination']))

                return True

    except Exception as e:
        print("OCURRIO UN ERROR: {}".format(e))
        return False

# storage_credentials_path: Path al archivo JSON que contiene las credenciales de service_account de storage
# bucketName: Nombre del segmento en cloud storage donde se subira el archivo
# project_name: Nombre del projecto de destino en BigQuery
# file_name: nombre del archivo a subir. Este debe estar en la carpeta codigo
# Sube el archivo a cloud storage utilizando las credenciales proporcionadas en el archivo
#         json (apis y servicios -> credenciales -> crear credenciales -> cuenta de servicio)
def upload_storage(storage_credentials_path, bucketName, project_name, file_name):
    print("SUBIENDO A STORAGE...")
    try:
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = storage_credentials_path
        client = storage.Client(project=project_name)
        bucket = client.get_bucket(bucketName)
        blob = bucket.blob(file_name, chunk_size=262144)
        blob.upload_from_filename(file_name)
        print("EL ARCHIVO SE SUBIO CORRECTAMENTE")
        files = bucket.list_blobs()
        fileList = [file.name for file in files if '.' in file.name]
        print("ARCHIVOS EN EL BUCKET: {}".format(fileList))
    except Exception as e:
        print("OCURRIO UN ERROR: {}".format(e))


def thread_function(total_time, start_time):
    aux = total_time - (time.time() - start_time)
    while aux >= 0:
        aux = round(total_time - (time.time() - start_time))
        print("TIEMPO RESTANTE: {} segundos".format(aux))
        time.sleep(1)


if __name__ == "__main__":
    print("\n\n\n-------------- creado por Jose Poblete C. --> Practicante Alianzas Estrategicas ----------------\n\n\n")
    aux = True
    print("QUE DESEAS HACER? \n 1) REALIZAR SOLO LA BAJADA A CSV \n 2) SOLO SUBIR UN ARCHIVO A STORAGE \n 3) BAJAR ARCHIVO Y SUBIR A STORAGE \n 4) SALIR")
    option = input("INGRESE LA OPCION: ")
    if option in ['1', '2', '3']:
        parsed_json = ''
        with open('config.json', 'r') as file:
            parsed_json = json.load(file)

        project_name = input(
            "INGRESE EL NOMBRE DEL PROYECTO EN BIGQUERY (exit para salir) (ej:so-cm-alianalytics-dev)(enter para usar el archivo config.json): ")
        if project_name != 'exit':
            if project_name == '':
                project_name = parsed_json["project_name"]
            print(project_name)
            query = False
            while query == False and option in ['1', '3']:
                query = input(
                    "INGRESA LA QUERY A CORRER (exit para salir) (ej: SELECT * FROM taxonomy LIMIT 10) (enter para usar el archivo config.json): ")
                if query != '' and query != 'exit':
                    aux = query.upper()
                    if 'FROM' not in aux or 'SELECT' not in aux or len(aux.split(' ')) < 3 or len(aux.split('FROM')[0].replace(' ', '')) < 7:
                        print("QUERY INCORRECTA, PRUEBE OTRA VEZ")
                        query = False
            if query != 'exit':
                if query == '':
                    query = parsed_json["query"]
                    print(query)
                if option in ['1']:
                    bq_credential_location = input(
                        "INGRESA EL PATH AL ARCHIVO JSON CON LAS CREDENTIALES OAuth 2.0 Client (exit para salir) (enter para usar el archivo config.json): ")
                    if bq_credential_location != 'exit':
                        if bq_credential_location == '':
                            bq_credential_location = parsed_json["bq_credentials_path"]
                        print(bq_credential_location)
                    else:
                        aux = False
                if aux:
                    if option in ['2', '3']:
                        storage_credential_location = input(
                            "INGRESA EL PATH AL ARCHIVO JSON CON LAS CREDENTIALES service_account (exit para salir) (enter para usar el archivo config.json): ")
                        if storage_credential_location != 'exit':
                            if storage_credential_location == '':
                                storage_credential_location = parsed_json["storage_credentials_path"]
                            print(storage_credential_location)
                            bucket_name = input(
                                "INGRESA EL NOMBRE DEL BUCKET EN STORAGE DONDE SE SUBIRA EL CSV (exit para salir)(enter para usar el archivo config.json): ")

                            if bucket_name != 'exit':
                                if bucket_name == '':
                                    bucket_name = parsed_json['bucket_name']
                                print(bucket_name)
                            else:
                                aux = False
                        else:
                            aux = False
                    if aux:
                        file_name = ''
                        if option in ['1', '3']:
                            file_name = input(
                                "INGRESA EL NOMBRE DEL CSV DE DESTINO (exit para salir)(enter para usar el archivo config.json): ")
                        else:
                            file_name = input(
                                "INGRESA EL NOMBRE DEL CSV A SUBIR (exit para salir)(enter para usar el archivo config.json): ")
                        if file_name != 'exit':
                            if file_name == '':
                                file_name = parsed_json['destination_csv_name']
                            file_name = file_name.split('.')[0] + '.csv'
                            print(file_name)
                            start_time = time.time()
                            cont = True
                            if option == '1':
                                cont = False
                                cont = bq_to_csv(
                                    bq_credential_location, project_name, file_name, query)
                            elif option == '3':
                                cont = False
                                cont = bq_to_storage(
                                    storage_credential_location,bucket_name, project_name, query)
                            if cont:
                                if option in ['1']:
                                    print("DATOS EXPORTADOS CORRECTAMENTE")
                                elif option in ['3']:
                                    print("DATOS EXPORTADOS CORRECTAMENTE A STORAGE")
                                if option in ['2']:
                                    size = os.path.getsize(file_name)
                                    print("SE SUBIRAN {}mb.  Tiempo estimado: {} segundos".format(
                                        round(size/1000000), round(size/1000000)*7))
                                    time_t = threading.Thread(target=thread_function, args=(
                                        round(size/1000000)*7, time.time()))
                                    time_t.daemon = True
                                    time_t.start()
                                    upload_storage(storage_credential_location, bucket_name,
                                                   project_name, file_name)
                            print("TIEMPO TOTAL: %s seg ---" %
                                  (time.time() - start_time))

    print("PROGRAMA TERMINADO")
